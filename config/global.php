<?php
/**
 * Created by PhpStorm.
 * User: Faisal
 * Date: 5/18/2018
 * Time: 12:00 PM
 */
return [
    'MEMBERME_ID' => env('MEMBERME_ID',15550),
    'LOGIN_URL' => env('LOGIN_URL','http://memberme.me/login.php'),
    'SUPER_ADMIN_REGISTRATION_EMAIL' => env('SUPER_ADMIN_REGISTRATION_EMAIL','registrations@memberme.me'),
];