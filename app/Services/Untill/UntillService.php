<?php
/**
 * Created by PhpStorm.
 * User: Faisal
 * Date: 12/14/2019
 * Time: 8:22 PM
 */

namespace App\Services\Untill;


use App\Exceptions\ApiException;
use App\Organization;
use App\UntillSetting;
use Dompdf\Exception;

class UntillService
{
    const TEMP_USERNAME = 'tempUsername';
    const TEMP_PASSWORD = 'tempPass';

    /** @var  $client Client */
    public $client;

    public function __construct(Organization $organization)
    {
        /** @var UntillSetting $untillSetting */
        $untillSetting = $organization->untillSetting;

        if(empty($untillSetting)){
            throw new ApiException(null,['error' => 'Please Setup Pos Setting First']);
        }

        if(empty($untillSetting->username) || empty($untillSetting->password) || empty($untillSetting->url) || empty($untillSetting->port))
            throw new ApiException(null,['error' => 'Please Complete Pos Setup First']);

        $this->client = new \App\Services\Untill\Client($untillSetting->username, $untillSetting->password, 'http://'.$untillSetting->url, $untillSetting->port);
    }



    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            $response = call_user_func_array(array($this, $method), $arguments);
        }
        $errorCode = $response['ReturnCode'][0];
        if ($errorCode == 0) {
            if ($method == 'getClients') {
                return array_get(array_get($response, 'Clients', []), 'item', []);
            }
            if ($method == 'addOrUpdateClient') {
                return array_get(array_get(array_get($response, 'Extra'), 'item'), 'Value');
            }
            if ($method == 'getClientById') {
                return array_get(array_get($response, 'Clients'), 'item');
            }
            return $response;
        }else{
            \Log::info($response);
            throw new ApiException(null,['error' => array_get($response,'ReturnMessage','Something wrong with POS server')]);
        }
    }

    /**
     * @return mixed|null|\SimpleXMLElement|array
     */
    private function getClients()
    {
        $xml = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TPAPIPosIntfU-ITPAPIPOS">
           <soapenv:Header/>
           <soapenv:Body>
              <urn:GetClients soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                 <Request xsi:type="urn:urn:TGetClientsRequest" xmlns:urn="urn:TPAPIPosIntfU">
                    <Password xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_PASSWORD . '</Password>
                    <UserName xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_USERNAME . '</UserName>
                 </Request>
              </urn:GetClients>
           </soapenv:Body>
        </soapenv:Envelope>';

        return $this->client->call($xml);
    }

    /**
     * @param array $data
     * @return mixed|null|\SimpleXMLElement
     */
    private function addOrUpdateClient($data = [])
    {
        $xml = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TPAPIPosIntfU-ITPAPIPOS" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
           <soapenv:Header/>
           <soapenv:Body>
              <urn:UpdateClients soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                 <Request xsi:type="urn:TUpdateClientsRequest" xmlns:urn="urn:TPAPIPosIntfU">
                    <Password xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_PASSWORD . '</Password>
                    <UserName xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_USERNAME . '</UserName>
                    <Clients xsi:type="urn1:TClientsArray" soapenc:arrayType="urn1:TClient[0]">
                        <item xsi:type="NS3:TClient">
                            <Id xsi:type="xsd:long">' . array_get($data, 'untill_id', 0) . '</Id>                          
                            <Name xsi:type="xsd:string">' . array_get($data, 'full_name') . '</Name>
                            <Phone xsi:type="xsd:string">' . array_get($data, 'contact_no') . '</Phone>
                            <Email xsi:type="xsd:string">' . array_get($data, 'email') . '</Email>
                            <Address xsi:type="xsd:string">' . array_get($data, 'address') . '</Address>
                           
                            <BirthDate xsi:type="xsd:dateTime">' . array_get($data, 'date_of_birth') . '</BirthDate>
                            <CardNumber xsi:type="xsd:dateTime">' . base64_encode(array_get($data, 'swipe_card') ). '</CardNumber>
                        </item>
                    </Clients>
                    <Extra xsi:type="urn1:TExtraInfoArray" soapenc:arrayType="urn1:TExtraInfo[]" xmlns:urn1="urn:TPAPIPosTypesU"/>
                 </Request>
              </urn:UpdateClients>
           </soapenv:Body>
        </soapenv:Envelope>';

        return $this->client->call($xml);
    }

    private function getClientById($untilId)
    {
        $xml = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TPAPIPosIntfU-ITPAPIPOS" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:GetClientsEx soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <Request xsi:type="urn:TGetClientsExRequest" xmlns:urn="urn:TPAPIPosIntfU">
            <Password xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_PASSWORD . '</Password>
            <UserName xsi:type="xsd:string">' . \App\Services\Untill\UntillService::TEMP_USERNAME . '</UserName> 
            <Extra xsi:type="urn1:TExtraInfoArray" soapenc:arrayType="urn1:TExtraInfo[]" xmlns:urn1="urn:TPAPIPosTypesU">
        		<item xsi:type="NS3:TExtraInfo">
                    <Key xsi:type="xsd:string">id</Key>
                    <Value xsi:type="xsd:string">'.$untilId.'</Value>
                </item>
            </Extra>
         </Request>
      </urn:GetClientsEx>
   </soapenv:Body>
</soapenv:Envelope>';

        return $this->client->call($xml);
    }
}