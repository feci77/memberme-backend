<?php

namespace App\Http\Controllers\Kiosk;

use App\KioskBackground;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class BackgroundController extends Controller
{
    public function create(Request $request)
    {
        $allowedImageExtensionArray = ['jpeg', 'bmp', 'png','JPG'];
        $validationRules = [
            'background' => 'mimes:' . implode(",", $allowedImageExtensionArray),
            'id' => 'exists:kiosk_backgrounds,id',
            'label' => 'string',
            'orientation' => 'between:1,2'
        ];

        $validator = Validator($request->all(), $validationRules);

        if ($validator->fails()) {
            return api_error($validator->errors());
        }

        $file = $request->file('background');
        if($file){
            $name = $file->getClientOriginalName();
            $name = md5($name).'.'.$file->getClientOriginalExtension();
            $path = '/kiosk-backgrounds/'.$name;
            Storage::put($path,File::get($file->getRealPath()));
            $url =  Storage::disk('local')->url($path);
        }

        if(isset($request->id) && !empty($request->id)){
            $kioskBackground = KioskBackground::find($request->id);
        }else{
            $kioskBackground = new KioskBackground();
        }

        $kioskBackground->label = $request->get('label');
        $kioskBackground->url = !empty($url)?$url:$kioskBackground->url;
        $kioskBackground->orientation = $request->get('orientation');
        $kioskBackground->save();
        $kioskBackground->refresh();

        return api_response($kioskBackground);
    }

    public function getList(Request $request)
    {
        $allBackgrounds = KioskBackground::limit(32)->orderBy('id','desc')->get();
        return api_response($allBackgrounds);
    }
}
