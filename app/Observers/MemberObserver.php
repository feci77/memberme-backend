<?php
/**
 * Created by PhpStorm.
 * User: Faisal
 * Date: 5/27/2019
 * Time: 1:05 PM
 */

namespace App\Observers;


use App\Member;

class MemberObserver
{
    /**
     * @param Member $member
     */
    public function saving(Member $member)
    {
        $middleName = (!empty($member->middle_name))? " $member->middle_name" :  "";
        $member->full_name = $member->first_name . $middleName .  " ". $member->last_name;    }

    /**
     * @param Member $member
     */
    public function updating(Member $member)
    {
        $middleName = (!empty($member->middle_name))? " $member->middle_name" :  "";
        $member->full_name = $member->first_name . $middleName .  " ". $member->last_name;
    }
}