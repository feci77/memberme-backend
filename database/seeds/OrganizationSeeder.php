<?php

use App\base\IStatus;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organization= new \App\Organization();
        $organization->name = 'Memberme';
        $organization->id = Config::get('MEMBERME_ID');
        $organization->current = IStatus::ACTIVE;
        $organization->status = IStatus::INACTIVE;
        $organization->save();

        $organization= new \App\Organization();
        $organization->name = 'Random Founders';
        $organization->status = IStatus::INACTIVE;
        $organization->save();

        $organization= new \App\Organization();
        $organization->name = 'Test Organization 2';
        $organization->status = IStatus::INACTIVE;
        $organization->save();

    }
}
